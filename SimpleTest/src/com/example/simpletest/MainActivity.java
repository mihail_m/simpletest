package com.example.simpletest;

import java.util.ArrayList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.TextView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.example.fragments.CodeFragment;
import com.example.fragments.FridgeFragment;
import com.example.fragments.ScanFragment;

public class MainActivity extends org.holoeverywhere.app.Activity {
	ActionBar bar;
	TextView actionTitle;
	CodeFragment codeFragment;
	ScanFragment scanFragment;
	FridgeFragment  fridgeFragment;
	FragmentTransaction fTrans;
	ViewFlipper flipper;
	ArrayList<ToggleButton> toggleButtons;
	ToggleButton tab1;
	ToggleButton tab2;
	ToggleButton tab3;
	ToggleButton tab4;
	ToggleButton tab5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		codeFragment = new CodeFragment();
		scanFragment = new ScanFragment();
		fridgeFragment = new FridgeFragment();
		initActionBar();
		initToggleButtons();
		runFragment(getResources().getString(R.string.tab_3));
	}
	
	private void initToggleButtons(){
		toggleButtons = new ArrayList<ToggleButton>();
		tab1 = (ToggleButton)findViewById(R.id.tab1);
		tab2 = (ToggleButton)findViewById(R.id.tab2);
		tab3 = (ToggleButton)findViewById(R.id.tab3);
		tab4 = (ToggleButton)findViewById(R.id.tab4);
		tab5 = (ToggleButton)findViewById(R.id.tab5);
		toggleButtons.add(tab1);
		toggleButtons.add(tab2);
		toggleButtons.add(tab3);
		toggleButtons.add(tab4);
		toggleButtons.add(tab5);
		tab1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
				{
					disableSelect(buttonView.getId());
				}
			}
		});
		tab2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
				{
					disableSelect(buttonView.getId());
					try {
						runFragment(buttonView.getText().toString());
					} catch (Exception e) {
					}
					actionTitle.setText(getResources().getString(R.string.tab_2));
				}
			}
		});
		tab3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
				{
					disableSelect(buttonView.getId());
					runFragment(buttonView.getText().toString());
					actionTitle.setText(getResources().getString(R.string.add_new));
				}
				
			}
		});
		tab4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
				{
					disableSelect(buttonView.getId());
				}
			}
		});
		tab5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
				{
					disableSelect(buttonView.getId());
				}
			}
		});
	}
	
	private void disableSelect(int id){
		for (ToggleButton toggle : toggleButtons) {
			if (id!=toggle.getId())
			{
				toggle.setChecked(false);
			}
		}
	}

	
	
	
	public void runFragment(String tabId){
	    fTrans = getSupportFragmentManager().beginTransaction();
		if (tabId.equals(getResources().getString(R.string.tab_3)))
		{
			  fTrans.replace(R.id.frgmCont, codeFragment);
		} else if (tabId.equals("scanFragment")){
			  fTrans.replace(R.id.frgmCont, scanFragment);
			  fTrans.addToBackStack(null);
		} else if (tabId.equals(getResources().getString(R.string.tab_2))){
			 fTrans.replace(R.id.frgmCont, fridgeFragment);
		}
		fTrans.commit();
	}

	private void initActionBar() {
		View customNav = LayoutInflater.from(this).inflate(R.layout.navigation_bar, null);
		actionTitle = (TextView) customNav.findViewById(R.id.title);
		actionTitle.setTypeface(FontMaker.getSegoeFont(this));
		bar = getSupportActionBar();
		bar.setCustomView(customNav);
		bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_gradient)); 
		bar.setHomeButtonEnabled(false);
		bar.setDisplayUseLogoEnabled(false);
		bar.setDisplayShowTitleEnabled(false);
		bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		flipper = (ViewFlipper)customNav.findViewById(R.id.flipper);
		Button backButton = (Button)customNav.findViewById(R.id.back_button);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		Button saveButton = (Button)customNav.findViewById(R.id.save_button);
		saveButton.setTypeface(FontMaker.getSegoeFont(this));
		backButton.setTypeface(FontMaker.getSegoeFont(this));
	}
	
	public void runScanning(){
		runFragment("scanFragment");
		flipper.setDisplayedChild(1);
		Drawable dr = getResources().getDrawable(R.drawable.scan_image);
		dr.setBounds(tab3.getCompoundDrawables()[1].getBounds());
		tab3.setCompoundDrawables(null,dr , null, null);
	}
	
	@Override
	public void onBackPressed() {
		if (flipper!=null&&flipper.getDisplayedChild()!=0&&flipper.getDisplayedChild()==1)
		{
			flipper.setDisplayedChild(0);
			Drawable dr = getResources().getDrawable(R.drawable.add_item);
			dr.setBounds(tab3.getCompoundDrawables()[1].getBounds());
			tab3.setCompoundDrawables(null,dr , null, null);
		}
		super.onBackPressed();
	}
	
	

	
}
