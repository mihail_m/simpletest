package com.example.simpletest;

import android.content.Context;
import android.graphics.Typeface;

public class FontMaker {
	private static Typeface segoeWP;
	private static Typeface segoeWPlight;
	private static Typeface segoe;
	
	public static Typeface getSegoeFont(Context ctx) {
		if (segoeWP == null)
		{
			segoeWP = Typeface.createFromAsset(ctx.getAssets(), "fonts/SegoeWP-Semibold.ttf");
		}
		return segoeWP;
	}
	
	public static Typeface getSegoeFontLight(Context ctx) {
		if (segoeWPlight == null)
		{
			segoeWPlight = Typeface.createFromAsset(ctx.getAssets(), "fonts/SegoeWP-Light.ttf");
		}
		return segoeWPlight;
	}
	
	public static Typeface getSegoe(Context ctx) {
		if (segoe == null)
		{
			segoe = Typeface.createFromAsset(ctx.getAssets(), "fonts/SegoeWP.ttf");
		}
		return segoe;
	}
}
