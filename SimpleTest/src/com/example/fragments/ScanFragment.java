package com.example.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.simpletest.FontMaker;
import com.example.simpletest.R;

public class ScanFragment extends Fragment {
	Button rightButton;
	Button leftButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.scan_view);
		leftButton = (Button)view.findViewById(R.id.left_button);
		leftButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				rightButton.setBackgroundResource(R.drawable.right_button_unselected);
				leftButton.setBackgroundResource(R.drawable.left_button_selected);
			}
		});
		rightButton = (Button)view.findViewById(R.id.right_button);
		rightButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rightButton.setBackgroundResource(R.drawable.right_button_selected);
				leftButton.setBackgroundResource(R.drawable.left_button_unselected);
			}
		});
		leftButton.setTypeface(FontMaker.getSegoeFontLight(getActivity()));
		rightButton.setTypeface(FontMaker.getSegoeFontLight(getActivity()));
		return view;
	}


}
