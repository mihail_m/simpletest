package com.example.fragments;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.TextView;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.simpletest.FontMaker;
import com.example.simpletest.MainActivity;
import com.example.simpletest.R;

public class CodeFragment extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.scan_page, null);
		ImageButton scanBut = (ImageButton)view.findViewById(R.id.scan_but);
		scanBut.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).runScanning();
			}
		});
		ImageButton mapBut = (ImageButton)view.findViewById(R.id.map_but);
		mapBut.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		TextView scanText = (TextView)view.findViewById(R.id.scan_but_text);
		scanText.setTypeface(FontMaker.getSegoeFont(getActivity()));
		TextView mapText = (TextView)view.findViewById(R.id.map_but_text);
		mapText.setTypeface(FontMaker.getSegoeFont(getActivity()));
		return view;
	}

}
