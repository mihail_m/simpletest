package com.example.fragments;

import java.util.ArrayList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.simpletest.FontMaker;
import com.example.simpletest.R;

public class FridgeFragment extends Fragment{
	
	ArrayList<ItemProduct> products;
	ProductAdapter adapter;
    private MyTouchListener mOnTouchListener;
    private int action_down_x = 0;
    private int action_up_x = 0;
    private int difference = 0;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fridge_view, null);
		products = new ArrayList<FridgeFragment.ItemProduct>();
		for (int i = 0; i < 20; i++) {
			ItemProduct product = new ItemProduct(i);
			products.add(product);
		}
		ListView list = (ListView)view.findViewById(R.id.list_view);
		IItemClickListener deleteRow = new IItemClickListener(){
			@Override
			public void onItemClicked(View v, int itemId) {
				products.remove(itemId);
				adapter.notifyDataSetChanged();
			}
		};
		adapter = new ProductAdapter(getActivity(),products,deleteRow);
		list.setAdapter(adapter);
		 mOnTouchListener = new MyTouchListener();
		return view;
	}
	
	public class ItemProduct{
		int countOfDays;
		boolean isNeedRemove;
		
		public boolean isNeedRemove() {
			return isNeedRemove;
		}
		
		public void setNeedRemove(boolean isNeedRemove) {
			this.isNeedRemove = isNeedRemove;
		}
		
		public ItemProduct(int countOfDays) {
			this.countOfDays = countOfDays;
		}
		
	}
	
	public class ProductAdapter  extends BaseAdapter{
		Context context;
		ArrayList<ItemProduct> products;
		LayoutInflater inflanter;
		IItemClickListener deleteRow;
		
		public ProductAdapter(Context context,ArrayList<ItemProduct> products,IItemClickListener deleteRow) {
			this.context = context;
			this.products = products;
			this.deleteRow = deleteRow;
			inflanter = LayoutInflater.from(context);
		}
		
		
		@Override
		public int getCount() {
			return products.size();
		}
		@Override
		public Object getItem(int arg0) {
			return products.get(arg0);
		}
		@Override
		public long getItemId(int arg0) {
			return 0;
		}
		@Override
		public View getView(int position, View view, ViewGroup arg2) {
			ViewHolder holder;
			if (view==null)
			{
				view = inflanter.inflate(R.layout.list_item_cell);
				holder = new ViewHolder();
				holder.product = (TextView)view.findViewById(R.id.name_of_product);
				holder.lastUsed = (TextView)view.findViewById(R.id.last_used);
				holder.daysLeft = (TextView)view.findViewById(R.id.digit);
				holder.textDaysLeft = (TextView)view.findViewById(R.id.days_left);
				holder.view = (View)view.findViewById(R.id.square);
				holder.product.setTypeface(FontMaker.getSegoe(context));
				holder.lastUsed.setTypeface(FontMaker.getSegoeFontLight(context));
				holder.daysLeft.setTypeface(FontMaker.getSegoe(context));
				holder.textDaysLeft.setTypeface(FontMaker.getSegoeFontLight(context));
				holder.deleteButton = (ImageView)view.findViewById(R.id.delete_button);
				view.setTag(R.id.square, holder);
			}	else {
				holder = (ViewHolder) view.getTag(R.id.square);
			}
			holder.deleteButton.setOnClickListener(new IItemClickListener.ItemViewClickListener(deleteRow, position));
			holder.deleteButton.setVisibility(View.GONE);
			view.setTag(position);
			ItemProduct item = (ItemProduct) getItem(position);
			holder.daysLeft.setText(((Integer)item.countOfDays).toString());
			switch (item.countOfDays) {
			case 0:
				holder.view.setBackgroundColor(getResources().getColor(R.color.zero_days_color));
				break;
			case 1:
				holder.view.setBackgroundColor(getResources().getColor(R.color.one_day_color));
				break;
			case 2:
				holder.view.setBackgroundColor(getResources().getColor(R.color.two_days_color));
				break;
			case 3:
				holder.view.setBackgroundColor(getResources().getColor(R.color.three_days_color));
				break;
			case 4:
				holder.view.setBackgroundColor(getResources().getColor(R.color.more_days_color));
				break;
			default:
				holder.view.setBackgroundColor(getResources().getColor(R.color.more_days_color));
				break;
			}
			if (item.countOfDays>0)
			{
				holder.textDaysLeft.setText(getResources().getString(R.string.days_left));
			} else {
				holder.textDaysLeft.setText(getResources().getString(R.string.stale));
			}
			view.setOnTouchListener(mOnTouchListener);
			return view;
		}
	}
	
	
	public interface IItemClickListener {
		public void onItemClicked(View v, int itemId);

		class ItemViewClickListener implements OnClickListener {
			private int itemId;
			private IItemClickListener listener;

			public ItemViewClickListener(IItemClickListener listener, int itemId) {
				this.itemId = itemId;
				this.listener = listener;
			}

			@Override
			public void onClick(View v) {
				listener.onItemClicked(v, itemId);
			}
		}
	}
	
	private class ViewHolder{
		TextView product;
		TextView lastUsed;
		TextView daysLeft;
		TextView textDaysLeft;
		View view;
		ImageView deleteButton;
		
	}
	
	
	  class MyTouchListener implements OnTouchListener
	    {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				ViewHolder holder = (ViewHolder) v.getTag(R.id.square);
				int action = event.getAction();
				int position = (Integer) v.getTag();

				switch (action) {
				case MotionEvent.ACTION_DOWN:
					action_down_x = (int) event.getX();
					Log.d("action", "ACTION_DOWN - ");
					break;
				case MotionEvent.ACTION_MOVE:
					Log.d("action", "ACTION_MOVE - ");
					action_up_x = (int) event.getX();
					difference = action_down_x - action_up_x;
					break;
				case MotionEvent.ACTION_UP:
					Log.d("action", "ACTION_UP - ");
					calcuateDifference(holder, position);
					action_down_x = 0;
					action_up_x = 0;
					difference = 0;
					break;
				}
				return true;
			}
	    }
	  
	  
	  private void calcuateDifference(final ViewHolder holder, final int position) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (difference == 0) {
					}
					if (difference > 75) {
						holder.deleteButton.setVisibility(View.VISIBLE);
					}
					if (difference < -75) {
						holder.deleteButton.setVisibility(View.GONE);
					}
				}
			});
		}

}
